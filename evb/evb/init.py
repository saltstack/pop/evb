def __init__(hub):
    # Remember not to start your app in the __init__ function
    # This function should just be used to set up the plugin subsystem
    # The run.py is where your app should usually start
    for dyne in []:
        hub.pop.sub.add(dyne_name=dyne)
    hub.pop.sub.load_subdirs(hub.evb)


def cli(hub):
    hub.pop.config.load(['evb'], cli="evb")
    # Your app's options can now be found under hub.OPT.evb
    kwargs = dict(hub.OPT.evb)

    # Initialize the asyncio event loop
    hub.pop.loop.create()

    # Start the async code
    coroutine = hub.evb.init.run(**kwargs)
    hub.pop.Loop.run_until_complete(coroutine)

async def run(hub, **kwargs):
    """
    This is the entrypoint for the async code in your project
    """
    event = {"tag": kwargs["tag"], "data": kwargs["data"]}
    await hub.evb.init.pub(kwargs["publishers"], event)

async def pub(hub, publishers, event):
    for bus in hub.evb.pub:
        if bus.name not in publishers:
            continue
        hub.pop.loop.ensure_future(f"hub.evb.pub.{bus.name}")(event)

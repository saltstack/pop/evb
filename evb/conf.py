# https://pop.readthedocs.io/en/latest/tutorial/quickstart.html#adding-configuration-data

# In this dictionary goes all the immutable values you want to show up under hub.OPT.evb
CONFIG = {
    "config": {
        "default": None,
        "help": "Load extra options from a configuration file onto hub.OPT.evb",
    },
    "data": {
        "default": None,
        "help": "The data to pass into the event bus",
        },
    "publishers": {
        "default": ["stdout"],
        "help": "The list of publishers to broadcast to",
        },
    "tag": {
        "default": "evb.test",
        "help": "The tag to give the event",
        },
}

# The selected subcommand for your cli tool will show up under hub.SUBPARSER
# The value for a subcommand is a dictionary that will be passed as kwargs to argparse.ArgumentParser.add_subparsers
SUBCOMMANDS = {
    # "my_sub_command": {}
}

# Include keys from the CONFIG dictionary that you want to expose on the cli
# The values for these keys are a dictionaries that will be passed as kwargs to argparse.ArgumentParser.add_option
CLI_CONFIG = {
    "config": {"options": ["-c"]},
    "publishers": {"options": ["-p"], "nargs": "*"},
    "tag": {"options": ["-t"]},
    "data": {"options": ["-d"]},
    # "my_option1": {"subcommands": ["A list of subcommands that exclusively extend this option"]},
    # This option will be available under all subcommands and the root command
    # "my_option2": {"subcommands": ["_global_"]},
}

# These are the namespaces that your project extends
# The hub will extend these keys with the modules listed in the values
DYNE = {'evb': ['evb']}
